#include <vector>
#include <algorithm> // std::copy

// I found it impossible to implement mergeSort in a simple and genric way (something like std::stable_sort is definitely not simple), so this is only for std::vector, not for any iterators.
namespace aisdi_sort
{
	template <typename T>
	void mergeVectors(std::vector<T> &vec1, std::vector<T> &vec2, std::vector<T> &destination) // vec1 and vec2 must be sorted
	{
		auto from1=vec1.begin(), from2=vec2.begin(), to=destination.begin();
		for(;;)
		{
			if(from1 == vec1.end())
			{
				while(from2 != vec2.end())
				{
					*to = *from2;
					++to;
					++from2;
				}
				return;
			}
			if(from2 == vec2.end())
			{
				while(from1 != vec1.end())
				{
					*to = *from1;
					++to;
					++from1;
				}
				return;
			}
			if(*from1 < *from2)
			{
				*to = *from1;
				++from1;
			}
			else
			{
				*to =* from2;
				++from2;
			}
			++to;
		}
	}

	template <typename T>
	void mergeSort(std::vector<T> &vector)
	{
		size_t size = vector.size();
		if(size<2)
			return;
		size /= 2;
		auto middle = vector.begin() + size;
		std::vector<T> divided1(size), divided2(vector.end() - middle); // in serious implementation these memory allocations should be optimized
		std::copy(vector.begin(), middle, divided1.begin());
		std::copy(middle, vector.end(), divided2.begin());
		mergeSort(divided1); // recursion
		mergeSort(divided2); // recursion
		mergeVectors(divided1, divided2, vector);
	}
} // namespace aisdi_sort
