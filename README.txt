Tomasz Nowak
Zadanie z ćwiczeń z AISDI (algorytmy i struktury danych).

Program zawiera implementacje quicksorta i merge sorta. Jest to tylko małe ćwiczenie - algorytmy zostały zaimplementowane w prostej wersji. Nie ma optymalizacji, takich jak przerwanie rekurecji i wykorzystanie innego algorytmu dla małej liczby elementów (w obu algorytmach), rozważna alokacja pamięci albo scalanie w miejscu (w merge sorcie), optymalizacja dostępów do kieszeni procesora (w quicksorcie). 

quickSort jest generyczny: działa dla dowolnych iteratorów, dla których występują: operatory +, -, *, ++, -- oraz operator< dla wartości zwracanej przez dereferencję iteratora (dla uproszczenia nie dodałem możliwości określenia dowolnej funkcji porównującej).
mergeSort działa tylko dla obiektów std::vector (przechowujących wartości dowolnego typu).

Kompilacja - make
Wywołanie programu - komunikat w przypadku wywołania programu bez argumentów wyjaśnia, jak uruchomić program:
Usage: ./sort with:
- 1 argument - number of elements (elemets will be chosen at random); sorting time will be shown
- many arguments - all array elements to be sorted; all elements will be written in sorted order.
