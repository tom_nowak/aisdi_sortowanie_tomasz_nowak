#include <iostream>
#include <ctime>
#include "quickSort.h"
#include "mergeSort.h"


namespace aisdi_sort
{
	template<typename Iterator>
	bool checkIfSorted(const Iterator begin, const Iterator end)
	{
		Iterator i=begin, j=begin+1;
		if(begin==end)
			return 1; // empty container is sorted
		while(j != end)
		{
			if(*j < *i)
				return 0;
			i = j;
			++j;
		}
		return 1;
	}

	template<typename Iterator>
	void writeContainer(const Iterator begin, const Iterator end)
	{
		for(Iterator i = begin; i != end; ++i)
			std::cout << *i << " ";
		std::cout << std::endl;
	}
} // namespace aisdi_sort


int main(int argc, char **argv)
{
	using namespace aisdi_sort;
	if(argc==1)
	{
		std::cout << "Usage: " << argv[0] << " with:\n"
		             "- 1 argument - number of elements (elemets will be chosen at random); sorting time will be shown\n"
		             "- many arguments - all array elements to be sorted; all elements will be written in sorted order.\n";
	        return 1;
	}
	std::vector<int> vectorQ, vectorM; // vector for quicksort, for merge sort
	if(argc==2)
	{	
		std::srand(std::time(0));
		int tmp = std::atoi(argv[1]);
		if(tmp<1)
		{
			std::cerr << "Argument must be positive!\n";
			return 1;
		}
		vectorQ.reserve(tmp);
		for(int i=0; i<tmp; ++i)
			vectorQ.push_back(std::rand());
		vectorM = vectorQ;
		clock_t t = std::clock();
		quickSort(vectorQ.begin(), vectorQ.end());
		t = std::clock() - t;
		clock_t t1 = std::clock();
		mergeSort(vectorM);
		t1 = std::clock() - t1;
		std::cout << "quickSort: sorted in " << ((float)t)/CLOCKS_PER_SEC << " seconds.\n"
		             "mergeSort: sorted in " << ((float)t1)/CLOCKS_PER_SEC << " seconds.\n";
	}
	else
	{
		vectorQ.reserve(argc);
		for(int i=1; i<argc; ++i)
			vectorQ.push_back(std::atoi(argv[i]));
		vectorM = vectorQ;
		quickSort(vectorQ.begin(), vectorQ.end()); 
		mergeSort(vectorM);
		std::cout << "quickSort: ";		
		writeContainer(vectorQ.begin(), vectorQ.end());
		std::cout << "mergeSort: ";		
		writeContainer(vectorM.begin(), vectorM.end());
	}	
	if(checkIfSorted(vectorQ.begin(), vectorQ.end()))
		std::cout << "quickSort: checkIfSorted - OK\n";
	else
		std::cout << "quickSort: checkIfSorted - ERROR - not sorted!\n";
	if(checkIfSorted(vectorM.begin(), vectorM.end()))
		std::cout << "mergeSort: checkIfSorted - OK\n";
	else
		std::cout << "mergeSort: checkIfSorted - ERROR - not sorted!\n";
}

