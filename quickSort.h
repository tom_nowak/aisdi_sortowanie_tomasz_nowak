#include <utility> // std::swap

// Implementation of quickSort is generic, with the only simplification - compare function is operator<
namespace aisdi_sort
{
	template<typename Iterator>
	void choosePivot(Iterator begin, Iterator end)
	{
		const size_t arraySize = end - begin;
		Iterator middle = begin + arraySize/2;
		--end; // end - address after the last element
		// there should be: *begin <= *end <= *middle (pivot will be at the end)
		if(*end < *begin)
			std::swap(*end, *begin);
		if(*middle < *end)
			std::swap(*middle, *end);
		if(*end < *begin)
			std::swap(*end, *begin);
	}

	template<typename Iterator>
	Iterator divideArray(Iterator begin, Iterator end)
	{
		Iterator pivot = end-1;
		// elements to the left from begin - less or equal pivot
		// elements to the right from end - greater or equal pivot
		end = pivot-1;
		++begin; // the way choosePivot works guarantees that before the loop *begin<*pivot..., so begin is preincremented
		while(end - begin > 0)
		{
			if(*pivot < *begin)
			{
				std::swap(*begin, *end);
				--end;
			}
			else
				++begin;
		}
		if(*end < *pivot) // check if the last element should go before or after pivot
			++end; // now for sure *end >= *pivot and end[-1] <= *pivot
		std::swap(*pivot, *end);
		return end;
	}
	

	template<typename Iterator>
	void quickSort(Iterator begin, Iterator end)
	{
		if(end-begin<2)
			return;
		choosePivot(begin, end);
		Iterator dividePoint = divideArray(begin, end);
		quickSort(begin, dividePoint); // recursion for elements less than pivot
		quickSort(dividePoint+1, end); // recursion for elements greater or equal pivot
	}
} // namepsace aisdi_sort
