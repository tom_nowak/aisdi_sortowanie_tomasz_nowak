CC = g++
OPT = -std=c++11 -Wall -Wextra -pedantic -O3
qsort: main.cpp quickSort.h mergeSort.h Makefile
	$(CC) $(OPT) main.cpp -o sort
